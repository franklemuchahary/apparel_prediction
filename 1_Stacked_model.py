
# coding: utf-8

# In[1]:

import pandas as pd
import numpy as np

import matplotlib.pyplot as plt
import seaborn as sns
import matplotlib.image as mimg
get_ipython().magic('matplotlib inline')
from PIL import Image
from scipy import misc

import os


# In[2]:

from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.linear_model import LogisticRegression
#from sklearn.multiclass import OneVsRestClassifier
#from xgboost import XGBClassifier
from sklearn.svm import SVC

from sklearn.cross_validation import train_test_split
from sklearn.model_selection import StratifiedKFold 
from sklearn.decomposition import PCA
from sklearn.grid_search import GridSearchCV
from sklearn.metrics import accuracy_score, precision_score, recall_score


# In[3]:

os.getcwd()


# ### utility functions

# In[4]:

#one hot encoding function

def one_hot_encoder(df_name, df_column_name, suffix=''):
    temp = pd.get_dummies(df_name[df_column_name]) #get dummies is used to create dummy columns
    df_name = df_name.join(temp, lsuffix=suffix) #join the newly created dummy columns to original dataframe
    df_name = df_name.drop(df_column_name, axis=1) #drop the old column used to create dummy columnss
    return df_name


# ### read all the images and convert into arrays containing pixel values
Read Train Dataset
# In[18]:

#read 
def read_image_paths(folder_path, start, end):
    img_list = []
    if(os.path.isdir(folder_path)): #check if the given folder name is a valid directory
        for i in range(start, end+1):
            path = folder_path+str(i)+".png"
            if(os.path.exists(path)): #check if the particular file exists
                img_list.append(path)
    return img_list


# In[ ]:

img_folder_path = "train_LbELtWX/train/"
img_paths = []

img_paths = read_image_paths(img_folder_path, 1, 60000)

#use list comprehension to read the images as arrays and store them in a list
images = [misc.imread(i, mode="RGB") for i in img_paths] 
images = np.asarray(images)

#save images array as npy file
np.save('train_images', images)


# In[24]:

if os.path.exists('train_images.npy'):
    images = np.load('train_images.npy')
    print('Loaded')


# In[15]:

#store the image shape 
image_size = np.asarray([images.shape[1], images.shape[2], images.shape[3]])
image_size


# In[16]:

#scale the image pixel values and store it in images_scaled
images_scaled = images/255


# Read Test Dataset

# In[19]:

test_img_folder_path = "test_ScVgIM0/test/"
test_img_paths = read_image_paths(test_img_folder_path, 60001, 70000)
test_images = [misc.imread(i, mode="RGB") for i in test_img_paths]
test_images = np.asarray(test_images)

np.save('test_images', test_images)


# In[25]:

if os.path.exists('test_images.npy'):
    test_images = np.load('test_images.npy')
    print('Loaded')


# In[26]:

test_images_scaled = test_images/255


# ### read the image labels and one hot encode the labels

# In[29]:

image_labels = pd.read_csv("train_LbELtWX/train.csv")
print(image_labels.head(), "\n\n")

#do one hot encoding
image_labels_encoded = one_hot_encoder(image_labels, 'label', 'lab')
image_labels_encoded = image_labels_encoded.iloc[:,1:]
print(image_labels_encoded.head())


# In[83]:

image_labels['label'].values


# In[17]:

#check to see if distribution of target labels are equal (if not equal we need to assign weights to classes)
plt.bar(image_labels['label'].value_counts().index, image_labels['label'].value_counts().values)


# ### ---------------------------------------------------------------------------------------------------------------------------------
# ### basic multiclass classification without using neural nets
# We will need to flatten the input arrays into a single dimension

# In[256]:

images_flat = [i.flatten() for i in images_scaled]
images_flat = pd.DataFrame(images_flat)


# In[257]:

images_flat.shape


# In[258]:

test_images_flat = [i.flatten() for i in test_images_scaled]
test_images_flat = pd.DataFrame(test_images_flat)


# Too large dimension. Will take too much time to train. Use PCA to reduce dimensionality

# In[336]:

pca = PCA(n_components=0.90) #should contain 90% of the variance.
pca.fit(images_flat)

print(pca.n_components_)


# In[337]:

images_flat_pca = pca.transform(images_flat)
test_images_flat_pca = pca.transform(test_images_flat)


# Train Test Split

# In[338]:

train_x1, test_x1, train_y1, test_y1 = train_test_split(images_flat_pca, image_labels.iloc[:,1:]['label'], 
                                                        test_size=0.25, random_state=333)


# Random Forest Classifier
# 
# Using grid search to fit the hyperparameters first in logarithmic scale to check for best vicinities and then in linear scale -> same for other classifiers too

# In[376]:

params = {'n_estimators':[500], 
          'bootstrap':[False], 
          'max_depth':[50],
         'max_features': ["log2"]}

rf = GridSearchCV(RandomForestClassifier(), params, n_jobs=-1, cv=3, scoring='accuracy', verbose=1)
rf = rf.fit(train_x1, train_y1)


# In[377]:

print(rf.best_params_, rf.best_score_)


# In[378]:

print(accuracy_score(rf.predict(test_x1), test_y1.values))
rf_preds1 = rf.predict_proba(test_images_flat_pca)
rf_probab_preds1 = rf.predict_proba(test_x1)


# Logistic Regression Classifier

# In[382]:

#logistic regression classifier
params = {'penalty':['l1'], 'C':[10]}

lr = GridSearchCV(LogisticRegression(), params, n_jobs=-1, cv=3, scoring='accuracy', verbose=1)
#lr = OneVsRestClassifier(LogisticRegression(), n_jobs=-1)
lr = lr.fit(train_x1, train_y1)


# In[383]:

print(lr.best_params_, lr.best_score_)


# In[384]:

print(accuracy_score(lr.predict(test_x1), test_y1.values))
lr_preds1 = lr.predict_proba(test_images_flat_pca)
lr_probab_preds1 = lr.predict_proba(test_x1)


# SVC

# In[372]:

params = {'C': np.linspace(10, 20, 1) ,'gamma': [0.0001], 'kernel': ['rbf'], 'probability': [True]}

svc = GridSearchCV(SVC(), params, n_jobs = -1, cv=3, scoring = 'accuracy', verbose=1)
svc.fit(train_x1, train_y1)

print(svc.best_params_, svc.best_score_)


# In[373]:

print(accuracy_score(svc.predict(test_x1), test_y1.values))
svc_preds1 = svc.predict_proba(test_images_flat_pca)
svc_probab_preds1 = svc.predict_proba(test_x1)


# Stacking 

# In[398]:

level2_x = pd.DataFrame(np.hstack((rf_probab_preds1, lr_probab_preds1, svc_probab_preds1)))
level2_test = pd.DataFrame(np.hstack((rf_preds1, lr_preds1, svc_preds1)))


# In[401]:

print(level2_x.shape)
print(level2_test.shape)


# Stack using Logistic Regression

# In[426]:

params = {'penalty':['l1'], 'C':np.linspace(12, 20, 1)}

lr_stk = GridSearchCV(LogisticRegression(), params, n_jobs=-1, cv=3, scoring='accuracy', verbose=1)
#lr = OneVsRestClassifier(LogisticRegression(), n_jobs=-1)
lr_stk = lr_stk.fit(level2_x, test_y1)

print(lr_stk.best_params_, lr_stk.best_score_)


# In[427]:

lr_stacked_preds = lr_stk.predict_proba(level2_test)


# ### ---------------------------------------------------------------------------------------------------------------------------------

# ### prepare submissions

# In[129]:

def prep_submissions(preds_array, file_name):
    preds_df = pd.DataFrame(preds_array)
    predicted_labels = preds_df.idxmax(axis=1) #convert back one hot encoding to categorical variabless
    submission = pd.read_csv("test_ScVgIM0/test.csv")
    submission['label'] = predicted_labels
    submission.to_csv(file_name, index=False)
    print(pd.read_csv(file_name).head())


# In[428]:

prep_submissions(lr_stacked_preds, "lr_stacked_submission2.csv")


# # ----------------------------------------------------------------------------------------
# # DEEP LEARNING
from keras.models import Sequential
from keras.layers import Dense, Conv2D, Activation, Dropout, Flatten, MaxPooling2D
from keras.callbacks import TensorBoard, EarlyStopping
from datetime import datetime
from keras.utils import np_utilstrain_x, test_x, train_y, test_y = train_test_split(images_scaled, image_labels_encoded.values, 
                                                   test_size=0.25)def cnn_model(size, num_cnn_layers):
    #FILTER = 32
    KERNEL = (3, 3)
    MIN_NEURONS = 20
    MAX_NEURONS = 120
    
    steps = np.floor(MAX_NEURONS / (num_cnn_layers))
    filters = np.arange(MIN_NEURONS, MAX_NEURONS, steps)
    filters = filters.astype(np.int32)
    
    model = Sequential()
    
    for i in range(num_cnn_layers):
        if i == 0:
            model.add(Conv2D(filters[i], KERNEL, input_shape=tuple(size)))
        else:
            model.add(Conv2D(filters[i], KERNEL))
            
    model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(Flatten())
    model.add(Dense(int(MAX_NEURONS/2), activation='relu'))
    model.add(Dense(10, activation='softmax'))
    
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
    
    print(model.summary())
    
    return model
    
    #print(steps)
    #print(neurons)
model = cnn_model(tuple(image_size), 2)EPOCHS = 5
BATCH_SIZE = 5000

#set early stopping criteria
PATIENCE = 2 #this is the number of epochs with no improvment after which the training will stop
early_stopping = EarlyStopping(monitor='loss', min_delta=0, patience=PATIENCE, verbose=0, mode='auto')# TensorBoard callback
LOG_DIRECTORY_ROOT = 'tensor'
now = datetime.utcnow().strftime("%Y%m%d%H%M%S")
log_dir = "{}/run-{}/".format(LOG_DIRECTORY_ROOT, now)
tensorboard = TensorBoard(log_dir=log_dir, write_graph=True, write_images=True)def fit_and_evaluate(model, t_x, val_x, t_y, val_y):
    model.fit(t_x, t_y, epochs=EPOCHS, batch_size=BATCH_SIZE, 
          callbacks=[early_stopping, tensorboard], verbose=2)
    print(model.evaluate(val_x, val_y))n_folds=3

for i in range(n_folds):
    print("Running Fold: ",i+1)
    t_x, val_x, t_y, val_y = train_test_split(train_x, train_y, test_size=0.1, 
                                               random_state = np.random.randint(1,1000, 1)[0])
    fit_and_evaluate(model, t_x, val_x, t_y, val_y)
    print("======="*12, end="\n\n")   model.evaluate(test_x, test_y)predictions = model.predict(test_images_scaled)prep_submissions(predictions, 'cnn_submit-1.csv')