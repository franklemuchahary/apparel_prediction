
# coding: utf-8

# In[1]:

import pandas as pd
import numpy as np

import matplotlib.pyplot as plt
import seaborn as sns
import matplotlib.image as mimg
get_ipython().magic('matplotlib inline')
from PIL import Image
from scipy import misc

import os


# In[2]:

from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.linear_model import LogisticRegression
#from sklearn.multiclass import OneVsRestClassifier
#from xgboost import XGBClassifier
from sklearn.svm import SVC

from sklearn.cross_validation import train_test_split
from sklearn.model_selection import StratifiedKFold 
from sklearn.decomposition import PCA
from sklearn.grid_search import GridSearchCV
from sklearn.metrics import accuracy_score, precision_score, recall_score


# In[3]:

get_ipython().system('ls /apparel/')


# ### utility functions

# In[4]:

#one hot encoding function

def one_hot_encoder(df_name, df_column_name, suffix=''):
    temp = pd.get_dummies(df_name[df_column_name]) #get dummies is used to create dummy columns
    df_name = df_name.join(temp, lsuffix=suffix) #join the newly created dummy columns to original dataframe
    df_name = df_name.drop(df_column_name, axis=1) #drop the old column used to create dummy columnss
    return df_name


# ### read all the images and convert into arrays containing pixel values
Read Train Dataset
# In[6]:

#read 
def read_image_paths(folder_path, start, end):
    img_list = []
    if(os.path.isdir(folder_path)): #check if the given folder name is a valid directory
        for i in range(start, end+1):
            path = folder_path+str(i)+".png"
            if(os.path.exists(path)): #check if the particular file exists
                img_list.append(path)
    return img_list

img_folder_path = "train_LbELtWX/train/"
img_paths = []

img_paths = read_image_paths(img_folder_path, 1, 60000)

#use list comprehension to read the images as arrays and store them in a list
images = [misc.imread(i, mode="RGB") for i in img_paths] 
images = np.asarray(images)

#save images array as npy file
np.save('train_images', images)
# In[5]:

if os.path.exists('/apparel/train_images.npy'):
    images = np.load('/apparel/train_images.npy')
    print('Loaded')


# In[7]:

#store the image shape 
image_size = np.asarray([images.shape[1], images.shape[2], images.shape[3]])
image_size


# In[8]:

#scale the image pixel values and store it in images_scaled
images_scaled = images/255


# Read Test Dataset
test_img_folder_path = "test_ScVgIM0/test/"
test_img_paths = read_image_paths(test_img_folder_path, 60001, 70000)
test_images = [misc.imread(i, mode="RGB") for i in test_img_paths]
test_images = np.asarray(test_images)

np.save('test_images', test_images)
# In[9]:

if os.path.exists('/apparel/test_images.npy'):
    test_images = np.load('/apparel/test_images.npy')
    print('Loaded')


# In[10]:

test_images_scaled = test_images/255


# ### read the image labels and one hot encode the labels

# In[11]:

image_labels = pd.read_csv("/apparel/train.csv")
print(image_labels.head(), "\n\n")

#do one hot encoding
image_labels_encoded = one_hot_encoder(image_labels, 'label', 'lab')
image_labels_encoded = image_labels_encoded.iloc[:,1:]
print(image_labels_encoded.head())


# In[12]:

image_labels['label'].values


# In[13]:

#check to see if distribution of target labels are equal (if not equal we need to assign weights to classes)
plt.bar(image_labels['label'].value_counts().index, image_labels['label'].value_counts().values)


# ### ---------------------------------------------------------------------------------------------------------------------------------
# ### basic multiclass classification without using neural nets
# We will need to flatten the input arrays into a single dimension

# ## ---------------------------------------------------------------------------------------------------------

# ### prepare submissions

# In[14]:

def prep_submissions(preds_array, file_name):
    preds_df = pd.DataFrame(preds_array)
    predicted_labels = preds_df.idxmax(axis=1) #convert back one hot encoding to categorical variabless
    submission = pd.read_csv("/apparel/test.csv")
    submission['label'] = predicted_labels
    submission.to_csv(file_name, index=False)
    print(pd.read_csv(file_name).head())


# # ----------------------------------------------------------------------------------------
# # DEEP LEARNING

# In[16]:

from keras.models import Sequential
from keras.layers import Dense, Conv2D, Activation, Dropout, Flatten, MaxPooling2D
from keras.callbacks import TensorBoard, EarlyStopping
from datetime import datetime
from keras.utils import np_utils


# In[17]:

train_x, test_x, train_y, test_y = train_test_split(images_scaled, image_labels_encoded.values, 
                                                   test_size=0.25)


# In[28]:

def cnn_model(size, num_cnn_layers):
    #FILTER = 32
    KERNEL = (3, 3)
    MIN_NEURONS = 20
    MAX_NEURONS = 120
    
    steps = np.floor(MAX_NEURONS / (num_cnn_layers))
    filters = np.arange(MIN_NEURONS, MAX_NEURONS, steps)
    filters = filters.astype(np.int32)
    
    model = Sequential()
    
    for i in range(num_cnn_layers):
        if i == 0:
            model.add(Conv2D(filters[i], KERNEL, input_shape=tuple(size)))
        else:
            model.add(Conv2D(filters[i], KERNEL))
         
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(Flatten())
    model.add(Dense(int(MAX_NEURONS), activation='relu'))
    model.add(Dropout(0.2))
    model.add(Dense(int(MAX_NEURONS/2), activation='relu'))
    model.add(Dense(10, activation='softmax'))
    
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
    
    print(model.summary())
    
    return model
    
    #print(steps)
    #print(neurons)


# In[30]:

model = cnn_model(tuple(image_size), 2)


# In[31]:

EPOCHS = 20
BATCH_SIZE = 200

#set early stopping criteria
PATIENCE = 3 #this is the number of epochs with no improvment after which the training will stop
early_stopping = EarlyStopping(monitor='loss', min_delta=0, patience=PATIENCE, verbose=0, mode='auto')


# In[32]:

def fit_and_evaluate(model, t_x, val_x, t_y, val_y):
    model.fit(t_x, t_y, epochs=EPOCHS, batch_size=BATCH_SIZE, callbacks=[early_stopping], verbose=1)
    print("CV: ", model.evaluate(val_x, val_y))


# In[33]:

n_folds=3

for i in range(n_folds):
    print("Running Fold: ",i+1)
    t_x, val_x, t_y, val_y = train_test_split(train_x, train_y, test_size=0.1, 
                                               random_state = np.random.randint(1,1000, 1)[0])
    fit_and_evaluate(model, t_x, val_x, t_y, val_y)
    print("======="*12, end="\n\n")   


# In[34]:

model.evaluate(test_x, test_y)


# In[35]:

predictions = model.predict(test_images_scaled)


# In[36]:

prep_submissions(predictions, 'cnn_submit-6.csv')

