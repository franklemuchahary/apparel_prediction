# README #

### Apparel Classification Practice Challenge on Analytics Vidhya

Multiclass classification of images of apparels like shirts, pants, shoes etc very similar to the famous MNIST Digit Recognition.
Built a stacked model along which gave an accuracy of 87.2 % on the public leaderboard.

Check out the problem statement and datasets here:
https://datahack.analyticsvidhya.com/contest/practice-problem-identify-the-apparels/

Datasets were not uploaded here because they were too large in size.

#### DEEP LEARNING -> CNN

Built a CNN based model consisting of 2 convolutional layers with (3,3) shaped filters followed by a (2,2) shaped max pooling layers
The two convolutional layers were followed by 2 Dense Layers.
3-fold cross validation was performed. Trained using a GPU on floyd hub. 
Data was converted to .npy files and loaded to floyd hub datasets. (Avaliable on "floyd_data_/apparel/")

ACCURACY WITH CNN -> 91.77 % on public leaderboard.


